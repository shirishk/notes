Route 53

IPv4 32bit
IPv6 128bit

SOA Records
The name of the server that supplied the data for the zone.
The administrator of the zone.
The current version of the data file.
The number of seconds a secondary name server should wait before checking for updates.
The number of seconds a secondary name server should wait before retrying a failed zone transfer.
The maximum number of seconds that a secondary name server can use data
before it must either be refreshed or expire.
The default number of seconds for the time-to-live file on resource records.

NS stand for Name Server records
These records used by top level Domain servers to direct traffic

An 'A' record is the fundamental type of DNS record
'A' stands for Address.
Thesee A record is used by a computer to transalate the name of the
domain to the IP address.

Alias Records : worked like CNAME
Map one DNS name to another.
A CNAME can't be used for naked domain name.

ELB's do not have pre-defined IPv4 addresses, you resolve to them using a DNS name.
Understand the difference between an Alias Record and a CNAME.
Given the choise, always choose an Alias record over a CNAME

# Registered a Domain name
NS = Name Server
SOA = Start of Athority

# Routing Policy
Select closest region

Simple routing Policy
Weighted routing Policy : 10% to Mumbai Zone 90% to Sydeney Zone
Latency routing Policy : Select lowest latency region
Failover routing Policy : HealthCheck Active/Passive
Geolocation routing Policy : Based on Geolocation 

Important 
ELB Do not have prefferd IPv4 addresses, You resolve to them using a DNS name
Difference between Alias Record (Uses Load Balancer, cloud front) and CNAME (Machine)
Use Alias Record over CNAME.
There is a limit of 50 domain names, however the limit can be raised.