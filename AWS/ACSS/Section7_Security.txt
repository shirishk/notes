# Building IAM Policies - Lab

# Using IAM Roles With EC2 (recap) - Lab

# S3 CLI & Regions

# Multifactor Authentication on AWS - Lab

# Security Token Service
    Grants users limited and temporary access to AWS resources. User can come 
    from three sources:

    => Federation (typically Active Directory)
        > Uses Security Assertion Markup Language (SAML)
        > Grants temporary access based off the users Active Directory credentials.
            Does not need to be a user in IAM
        > Single sign on allows users to log in to AWS console without assigning 
            IAM credentials
    => Federation with Mobile Apps
        > Use Facebook/Amazon/Google or other OpenID providers to log in.
    => Cross Account access
        > Let's users from one AWS account access resources in another.

    Key Terms:
    => Federation: combining or joining a list of users in one domain (such as IAM)
        with a list of users in another domain (such as Active Directory, Facebook etc )
    => Identity Broker: a service that allows you to take an identity from point A 
        and join it (federate it) to point B 
    => Identity Store: Services like Active Directory, Facebook, Google etc
    => Identities: A user of a service like facebook etc.

    Scenario: You are hosting a company website on some EC2 web servers in your VPC.
    Users of the website must log in to the site which then authenticates against
    the companies active directory servers which are based on site at the companies
    head quarters. Your VPC is connected to your company HQ via a secure IPSEC VPN.
    Once logged in the user can only have access to their own S3 bucket.
    How you set this up?

    1. Employee enter their username and password
    2. The application calls an Identity Broker. The broker captures the username and
        password
    3. The Identity Broker uses the organization's LDAP directory to validate the 
        employee's identity
    4. The Identity Broker calls the new GetFederationToken function using IAM
        credentials. The call must include an IAM policy and a duration 
        (1 to 36 hours) along with a policy that specifies the permissions
        to be granted to the temporary security credentials
    5. The Security Token Service confirms that the policy of the IAM user making
        the call to GetFederationToken gives permission to create new tokens and then
        return four values to the application: As access key, a secret access key, a 
        token, and a duration (the token's lifetime)
    6. The Identity Broker return the temporary security credentials to the reporting
        application.
    7. The data storage application uses the temporary security credentials (including 
        the token) to make requests to Amazon S3.
    8. Amazon S3 uses IAM to verify that the credentials allow the requested operation
        on the given S3 bucket and key
    9. IAM provides S3 with the go-ahead to perform the requested operation.

    => In the Exam
        > Develop an Identity Broker to communicate with LDAP and AWS STS
        > Identity Broker always authenticates with LDAP first, THEN with 
            AWS STS
        > Application then gets temporary access to AWS resources

    Scenario 2
        > Develop an Identity Broker to communicate with LDAP and AWS STS
        > Identity Broker always authenticates with LDAP first, gets an 
            IAM Role associate with a user.
        > Application then authenticates with STS and assumes that IAM Role
        > Application uses that IAM role to interact with S3 

# Security On AWS
    => Shared Security Model
        AWS is responsible for securing the underlying infrastructure that supports 
        the cloud. You're responsible for anything you put on the cloud or connect to 
        the cloud.
    => AWS Security Responsibilities
        Amazon Web services is responsible for protecting the global infrastructure
        that runs all of the services offered in the AWS cloud. This infrastructure
        is comprised of the hardware, software, networking, and facilities that runs
        AWS services.

        AWS is responsible for the security configuration of its products that are
        considered managed services. Examples of these types of services include 
        Amazon DynamoDB, Amazon RDS, Amazon Redshift, Amazon Ekastic MapReduce,
        Amazon WorkSpaces.

    => Customer Security Responsibilities
        IAAS - such as Amazon EC2, Amazon VPC, and Amazon S3 are completely under your
        control and require you to perform all of the necessary security configuration
        and management tasks.

        Managed Service at AWS is responsible for patching, antivirus etc, however you 
        are responsible for account management and user access. Its recommended that MFA 
        be implemented, communicate to these services using SSL/TLS and that API/user 
        activity logging be setup with CloudTrail.

    => Storage Decommissioning
        When a storage device has reached the end of its useful life, AWS procedures
        include a decommissioning process that is designed to prevent customer data
        from being exposed to unauthorized individuals. AWS uses the techniques 
        detailed in DoD 5220.22-M ("National Industrial Security Program Operating Manual")
        or NIST 800-88("Guidelines for Media Sanitization") to destroy data as part of the 
        decommissioning process. All decommissioned magnetic storage devices are degaussed
        and physically destroyed in accordance with industry-standard practices.

    => Network Security
        Transmission Protection - you can connect to an AWS access point via HTTP or HTTPS
        using secure sockets layer (SSL), a cryptographic protocol that is designed to protect 
        against eavesdropping, tampering, and message forgery.
        For customers who require additional layers of network security, AWS offers the Amazon
        Virtual Private Cloud (VPC), which provides a private subnet within the AWS cloud, and 
        the ability to use an IPsec Virtual Private Network (VPN) device to provide an encrypted
        tunnel between the amazon VPC and your data center.
        Amazon corporate segregation - Logically, the AWS production network is segregated from
        the Amazon Corporate network by means of a complex set of network security / segregation
        devices.

    => Network Monitoring and Protection
        > DDoS
        > Man in the middle attack (MITM)
        > Ip Spoofing : The AWS-controlled, host-based firewall infrastructure will not permit an
            instance to send traffic with a source IP or MAC address other than its own.
            Unauthorized port scans by Amazon EC2 customers are a violation of the AWS Acceptable
            Use policy. You may request permission to conduct vulnerability scans as required to
            meet your specific compliance requirements. These scans must be limited to your own 
            instances and must not violet the AWS Acceptable Use Policy. You must request a 
            vulnerability scan in advance.

        > Port Scanning
        > Packet sniffing by other tenants

    => AWS Trusted Advisor: Trusted Advisor inspects your AWS environment and makes recommendation
        when opportunities may exist to save money, improve system performance, or close security
        gaps. It provides alerts on several of the most common security misconfigurations that can 
        occur, including leaving certain ports open that make you vulnerable to hacking and 
        unauthorized access, neglecting to create IAM accounts for your internal users, allowing
        public access to Amazon S3 buckets, not turning on user activity logging (AWS CloudTrail),
        or not using MFA on your root AWS Account.

    => Instance Isolation: Different instance running on the same physical machines are isolated 
        from each other via the Xen hypervisor. In addition, the AWS firewall resides within the
        hypervisor layer, between the physical network interface and the instance's virtual 
        interface. All packets must pass through this layer, thus an instance's neighbours have
        no more access to that instance than any other host on the Internet and can be treated as
        if they are on separate physical hosts. The physical RAM is separated using similar 
        mechanisms.

        Customer instances have no access to raw disk devices, but instead are presented with 
        virtualized disks. The AWS proprietary disk virtualization layer automatically resets 
        every block storage used by the customer, so that one customer's data is never 
        unintentionally exposed to another. In addition, memory allocated to guests is scrubbed
        (set to zero) by the hypervisor when it is unallocated to a guest. The memory is not 
        returned to the pool of free memory available for new allocations until the memory 
        scrubbing is complete.

    => Other Considerations:
        > Guest Operating System - virtual instances are completely controlled by you, the 
            customer. You have full root access or administrative control over accounts,
            services, and applications. AWS does not have any access rights to your instances
            or the guest OS.
        > Firewall - Amazon EC2 provides a complete firewall solution; this mandatory inbound 
            firewall is configured in a default deny-all mode and Amazon EC2 customers must
            explicitly open the ports needed to allow inbound traffic.
        > Guest Operating System - Encryption of sensitive data is generally a good security
            practice, and AWS provides the ability to encrypt EBS volumes and their snapshots
            with AWS-256. The encryption occurs on the servers that host the EC2 instances,
            providing encryption of data as it moves between EC2 instances and EBS storage.
            In order to be able to do this efficiently and with low latency, the EBS encryption
            feature is only available on EC2's more powerful instance types (e.g. M3, C3, R3, G2)

        > Elastic Load Balancing - SSL Termination on the load balancer is supported.
            Allows you to identify the originating IP address of a client connecting to your servers,
            whether you're using HTTPS ot TCP load balancing.
        
        > Direct Connect - Bypass Internet service providers in your network path. You can procure rack
            space within the facility housing the AWS Direct Connect location and deploy your equipment
            nearby. Once deployed, you can connect this equipment to AWS Direct Connect using a 
            cross-connect.
            Using industry standard 802.1q VLANs, the dedicated connection can be partitioned into 
            multiple virtual interfaces. This allows you to use the same connection to access public 
            resources such as objects stored in Amazon S3 using public IP address space, and private 
            resources such as Amazon EC2 instances running within an Amazon VPC using private IP space,
            while maintaining network separation between the public and private environments.
            

# AWS & IT Audits
    => Your organization may undergo an audit. This could be for PCI Compliance, ISO 27001, SOC etc.
    There is a level of shared responsibility in regards to audit:
    > AWS Provides - their annual certifications and reports (ISO 27001, PCI-DSS certification etc)
        Amazon are responsible the global infrastructure including all hardware, datacenrtes, physical 
        security etc.
    > Customer provides - everything they have put on AWS, such as EC2 instances, RDS instances, 
        Applications, Assets in S3 etc. Essentially the organization AWS assets (this can include the 
        data itself) 

# Security Quiz