sudo apt-get install VirtualBox-5.1

vagrant up --provider=virtualbox

|==>>> cat hosts.ini
testserver ansible_ssh_host=127.0.0.1 ansible_ssh_port=2222 ansible_ssh_user=vagrant ansible_ssh_private_key_file=.vagrant/machines/default/virtualbox/private_key

ping hosts
ansible testserver -i hosts.ini -m ping -vvvv

Run playbook
ansible-playbook web-nolts.yml

Retry
ansible-playbook web-nolts.yml --limit web-nolts.retry

Page 45