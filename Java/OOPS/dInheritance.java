// Inheritance is the object oriented programming concept where an object is based 
// on another object. Inheritance is the mechanism of code reuse. The object that 
// is getting inherited is called superclass and the object that inherits the 
// superclass is called subclass.


package com.journaldev.java.examples1;

class SuperClassA {

	public void foo(){
		System.out.println("SuperClassA");
	}
	
}

class SubClassB extends SuperClassA{
		
	public void bar(){
		System.out.println("SubClassB");
	}
	
}

public class Test {
	public static void main(String args[]){
		SubClassB a = new SubClassB();
		
		a.foo();
		a.bar();
	}
}
