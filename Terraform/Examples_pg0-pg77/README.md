# Terraform example from pages: 0 - 80
## Configure ACCESS_KEY_ID and SECRET_ACCESS_KEY
$> export AWS_ACCESS_KEY_ID= \
$> export AWS_SECRET_ACCESS_KEY=

## Create a .gitignore file and add following entries
.terraform/ \
*.tfstate \
*.tfstate.backup