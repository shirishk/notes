# Declare provider
provider "aws" {
    region = "ap-south-1"
}

# Declare variables
variable "server_port" {
    description = "The port use by the server for HTTP requests"
    default = 80
}

# Create security group
resource "aws_security_group" "web" {
    name = "sg1-single-server"

    ingress {
        from_port   = "${var.server_port}"
        to_port     = "${var.server_port}"
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }
    # Create a replacement resource before destroying the original rsource.
    lifecycle{
        create_before_destroy = true
    }
}

# Create a single instance
resource "aws_instance" "single-server" {
    ami = "ami-00b6a8a2bd28daf19"
    instance_type = "t2.micro"
    key_name = "mumbai"
    vpc_security_group_ids = ["${aws_security_group.web.id}"]
    user_data = <<-EOF
                #!/bin/bash
                yum update -y
                amazon-linux-extras install -y lamp-mariadb10.2-php7.2 php7.2
                yum install -y httpd mariadb-server
                sed  -i 's/Listen 80/Listen ${var.server_port}/g' /etc/httpd/conf/httpd.conf
                systemctl start httpd
                systemctl enable httpd
                usermod -a -G apache ec2-user
                chown -R ec2-user:apache /var/www
                chmod 2775 /var/www
                find /var/www -type d -exec chmod 2775 {} \;
                find /var/www -type f -exec chmod 0664 {} \;
                echo "<?php phpinfo(); ?>" > /var/www/html/phpinfo.php
                echo "Hello World from Terraform Script!" > /var/www/html/index.html
                EOF
    tags{
        Name = "A-Single-Server"
    }
# Create a replacement resource before destroying the original rsource.
    lifecycle{
        create_before_destroy = true
    }
}

# Output public link
output "public_url" {
  value = "http://${aws_instance.single-server.public_ip}:${var.server_port}"
}
