# Declare provider
provider "aws" {
    region = "ap-south-1"
}

# Declare variables
variable "server_port" {
    description = "The port use by the server for HTTP requests"
    default = 80
}

# Create security group
resource "aws_security_group" "elb" {
    name = "sg1-single-server"

    ingress {
        from_port   = "${var.server_port}"
        to_port     = "${var.server_port}"
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }
    # Create a replacement resource before destroying the original rsource.
    lifecycle{
        create_before_destroy = true
    }
}

# Create a launch configuration
resource "aws_launch_configuration" "launch-config" {
    image_id = "ami-00b6a8a2bd28daf19"
    instance_type = "t2.micro"
    key_name = "mumbai"
    security_groups = ["${aws_security_group.elb.id}"]
    user_data = <<-EOF
                #!/bin/bash
                yum update -y
                amazon-linux-extras install -y lamp-mariadb10.2-php7.2 php7.2
                yum install -y httpd mariadb-server
                sed  -i 's/Listen 80/Listen ${var.server_port}/g' /etc/httpd/conf/httpd.conf
                systemctl start httpd
                systemctl enable httpd
                usermod -a -G apache ec2-user
                chown -R ec2-user:apache /var/www
                chmod 2775 /var/www
                find /var/www -type d -exec chmod 2775 {} \;
                find /var/www -type f -exec chmod 0664 {} \;
                echo "<?php phpinfo(); ?>" > /var/www/html/phpinfo.php
                echo "Hello World from Terraform Script!" > /var/www/html/index.html
                EOF
                    
# Create a replacement resource before destroying the original rsource.
    lifecycle{
        create_before_destroy = true
    }
}

# Add all availble availability zones
data "aws_availability_zones" "all" {}

# Add elastic load balancer with health check
resource "aws_elb" "elb" {
  name = "elb-terraform"
  availability_zones = ["${data.aws_availability_zones.all.names}"]
  security_groups = ["${aws_security_group.elb.id}"]

  listener {
      lb_port = 80
      lb_protocol = "http"
      instance_port = "${var.server_port}"
      instance_protocol = "http"
  }
  health_check {
      healthy_threshold = 2
      unhealthy_threshold = 2
      timeout = 3
      interval = 30
      target = "HTTP:${var.server_port}/"
  }
}

# Add Auto scaling group ASG

resource "aws_autoscaling_group" "asg" {
    launch_configuration = "${aws_launch_configuration.launch-config.id}"
    availability_zones = ["${data.aws_availability_zones.all.names}"]
    
    load_balancers = ["${aws_elb.elb.name}"]
    health_check_type = "ELB"

    min_size = 2
    max_size = 5
    tag {
        key = "Name"
        value = "asg-terraform"
        propagate_at_launch = true
    }
}

# Output public link
output "public_url_elb_dns_name" {
  value = "http://${aws_elb.elb.dns_name}"
}
