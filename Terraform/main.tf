# Declare provider
provider "aws" {
    region = "ap-south-1"  
}

# Create a bucket to save the state of terraform script
resource "aws_s3_bucket" "s3_state" {
    bucket = "shirishk-terraform-state"

    versioning {
        enabled = true
    }

    lifecycle {
        prevent_destroy = false
    }
}

# Output the created bucket
output "s3_bucket_arn" {
  value = "${aws_s3_bucket.s3_state.arn}"
}