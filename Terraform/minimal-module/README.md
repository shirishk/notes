# Terraform
Prerequisites: 
1. Add AWS key
```
export AWS_ACCESS_KEY_ID=
export AWS_SECRET_ACCESS_KEY=
export AWS_REGION=<Your Region>
```
2. Repelace ```instance_key_name = "mumbai"``` with your key in ```<Your Region>``` from main.tf
___
## terraform init
Initialize a new or existing Terraform configuration
___
## terraform plan
Generate and show an execution plan \
get updates: \
```terraform plan -out dev-vpc.plan```
___
## terraform apply
Builds or changes infrastructure \
apply updates: \
```terraform apply dev-vpc.plan```
___
## terraform refresh
Update local state file against real resources
___
## terraform destroy
Destroy Terraform-managed infrastructure
___
## terraform -install-autocomplete
To add the necessary commands to your shell profile
___
___