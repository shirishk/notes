resource "aws_instance" "ec2" {
    ami             = "${var.instance_ami}"
    instance_type   = "${var.instance_instance_type}"
    key_name        = "${var.instance_key_name}"
    subnet_id       = "${var.instance_subnet_id}"
    vpc_security_group_ids = ["${aws_security_group.sg.id}"]
    user_data       = "${var.instance_user_data}"
    tags{
        Name        = "${var.instance_tag_name}"
    }
}
resource "aws_security_group" "sg" {
	name            = "${var.namespace}"
	description     = "Security group for ${var.namespace}"
	vpc_id          = "${var.security_groups_vpc_id}"
    tags{
        Name        = "${var.namespace}"
    }
}
resource "aws_security_group_rule" "ingress" {
    description             = "Added port in ingress ${var.sg_rule_ingress_ports[count.index]}"
    count                   = "${length(var.sg_rule_ingress_ports)}"
    type                    = "ingress"
    from_port               = "${var.sg_rule_ingress_ports[count.index]}"
    to_port                 = "${var.sg_rule_ingress_ports[count.index]}"
    protocol                = "tcp"
    cidr_blocks             = ["0.0.0.0/0"]
    security_group_id       = "${aws_security_group.sg.id}"
}
resource "aws_security_group_rule" "egress" {
    description             = "Added ports in egress 0-65535"
    type            = "egress"
    from_port       = "0"
    to_port         = "65535"
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
    security_group_id = "${aws_security_group.sg.id}"
}