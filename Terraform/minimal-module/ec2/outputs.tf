output "sg_id" {
    description   = "Security group id ${aws_security_group.sg.id}"
    value         = "${aws_security_group.sg.id}"
}
output "vpc_id" {
    description   = "VPC id ${aws_security_group.sg.vpc_id}"
    value         = "${aws_security_group.sg.vpc_id}"
}