variable "instance_ami" {
    description = "Amazon machine id"
    default     = "ami-00b6a8a2bd28daf19"
}
variable "instance_instance_type" {
    description = "Amazon instance type"
    default     = "t2.micro"
}
variable "instance_key_name" {
    description = "Instance key name"
    default     = "mumbai"
}
variable "instance_subnet_id" {
    description = "Instance Subnet Id"
}
variable "instance_user_data" {
    description = "Instance bootstrap script"
    default     = "hostname"
}
variable "namespace" {
    description = "Add tag Name"
}
variable "instance_tag_name" {
    description = "Instance tag Name"
    default     = "default-instance"
}
variable "security_groups_vpc_id" {
    description = "vpc id for the security group"
}
variable "sg_rule_ingress_ports" {
    description = "Security group rules"
    default     = []
}

