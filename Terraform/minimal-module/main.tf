provider "aws" {}

module "myvpc" {
    source                  = "vpc"
    namespace               = "DEV_VPC"
}
module "wordpress" {
    source                  = "ec2"
    namespace               = "wordpress"
    instance_ami            = "ami-00b6a8a2bd28daf19"
    instance_instance_type  = "t2.micro"
    instance_key_name       = "mumbai"
    instance_subnet_id      = "${module.myvpc.subnet-public[0]}"
    instance_user_data      = "${file("${path.module}/user_data/install_wordpress.sh")}"
    instance_tag_name       = "Wordpress-Server"
    security_groups_vpc_id  = "${module.myvpc.id}"
    sg_rule_ingress_ports   = ["22","80"]
}

module "jenkins" {
    source                  = "ec2"
    namespace               = "jenkins"
    instance_ami            = "ami-00b6a8a2bd28daf19"
    instance_instance_type  = "t2.micro"
    instance_key_name       = "mumbai"
    instance_subnet_id      = "${module.myvpc.subnet-public[0]}"
    instance_user_data      = "${file("${path.module}/user_data/install_jenkins.sh")}"
    instance_tag_name       = "Jenkins-Server"
    security_groups_vpc_id  = "${module.myvpc.id}"
    sg_rule_ingress_ports   = ["22","8080"]
}