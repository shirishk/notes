resource "aws_s3_bucket" "mybucket" {
  bucket = "rds_config"  
}
resource "aws_s3_bucket_object" "bucket" {
  bucket = "${aws_s3_bucket.b.id}"
  key = "new_object_key"
  source = "/root/S3/accountdetails"
  etag = "${md5(file("/root/S3/accountdetails"))}"
}
