#!/bin/bash
sudo yum update -y
sudo amazon-linux-extras install -y lamp-mariadb10.2-php7.2 php7.2
sudo yum install -y httpd

#Use the systemctl command to configure the Apache web server to start at each system boot.
sudo systemctl enable httpd

# Install Wordpress
wget https://wordpress.org/latest.tar.gz
tar -xzf *.tar.gz
cp -r wordpress/* /var/www/html/

sed -i 's/AllowOverride None/AllowOverride All/g' /etc/httpd/conf.d/welcome.conf

# Add your user (in this case, ec2-user) to the apache group
sudo usermod -a -G apache ec2-user

# Change the group ownership
sudo chown -R ec2-user:apache /var/www

# directory permissions of /var/www and its subdirectories
sudo chmod 2775 /var/www && find /var/www -type d -exec sudo chmod 2775 {} \;

# subdirectories permissions
find /var/www -type f -exec sudo chmod 0664 {} \;

# Restart httpd
sudo systemctl restart httpd