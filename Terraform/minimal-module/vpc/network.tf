# Create a VPC to launch our instances into
resource "aws_vpc" "myvpc" {
  cidr_block           = "${var.vpc_cidr_block}"
  enable_dns_hostnames = true

  tags {
    "Name" = "${var.namespace}"
  }
}

# Create an internet gateway to give our subnet access to the outside world
resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.myvpc.id}"

  tags {
    "Name" = "${var.namespace}-igw"
  }
}

# Grant the VPC internet access on its main route table
resource "aws_route" "internet_access" {
  route_table_id         = "${aws_vpc.myvpc.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.igw.id}"
}

# Grab the list of availability zones
data "aws_availability_zones" "available" {}

# Create a public subnet
resource "aws_subnet" "public" {
  count                   = "${length(var.cidr_blocks_public)}"
  vpc_id                  = "${aws_vpc.myvpc.id}"
  availability_zone       = "${data.aws_availability_zones.available.names[count.index]}"
  cidr_block              = "${var.cidr_blocks_public[count.index]}"
  map_public_ip_on_launch = true

  tags {
    "Name" = "${var.namespace}-public"
  }
}

# Create a private subnet
resource "aws_subnet" "private" {
  count                   = "${length(var.cidr_blocks_private)}"
  vpc_id                  = "${aws_vpc.myvpc.id}"
  availability_zone       = "${data.aws_availability_zones.available.names[count.index]}"
  cidr_block              = "${var.cidr_blocks_private[count.index]}"
  tags {
    "Name" = "${var.namespace}-private"
  }
}