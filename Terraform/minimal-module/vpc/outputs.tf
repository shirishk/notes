output "id" {
  value = "${aws_vpc.myvpc.id}"
}

output "subnet-public" {
  value = ["${aws_subnet.public.*.id}"]
}

output "subnet-private" {
  value = ["${aws_subnet.private.*.id}"]
}