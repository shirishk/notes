variable "namespace" {
  description = "Add tag Name"
}

variable "vpc_cidr_block" {
  description = "The top-level CIDR block for the VPC."
  default     = "10.1.0.0/16"
}

variable "cidr_blocks_public" {
  description = "The CIDR blocks to create the workstations in."
  default     = ["10.1.1.0/24"]
}

variable "cidr_blocks_private" {
  description = "The CIDR blocks to create the workstations in."
  default     = ["10.1.2.0/24"]
}